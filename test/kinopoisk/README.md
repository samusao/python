Simple of how a mini-kinopoisk works in Django:

### Models:
1. Genres
2. Movie
3. Review
4. Comment

### Stack:

- Django - 3.2.4
- DjangoRestFramework - 3.12.4
- psycopg2 - 2.9.1
- PostgreSQL