from django.db import models


class Genres(models.Model):
    genre = models.CharField(max_length=16, verbose_name='Жанр', blank=True, null=True)

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'

    def __str__(self):
        return f'{self.genre}'


class Movie(models.Model):
    name = models.CharField(max_length=64, verbose_name='Название фильма', blank=True, null=True)
    genre = models.ManyToManyField(Genres, verbose_name='Жанр фильма', null=False)
    duration = models.TimeField(verbose_name='Длительность')

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'

    def __str__(self):
        return f'{self.name}'


class Review(models.Model):
    film = models.ForeignKey(Movie, verbose_name='Фильм', null=False, on_delete=models.CASCADE)
    text = models.TextField(verbose_name='Текст рецензии', max_length=2000)

    class Meta:
        verbose_name = 'Рецензия'
        verbose_name_plural = 'Рецензии'

    def __str__(self):
        return f'{self.film} - [{self.text}]'


class Comment(models.Model):
    review = models.ForeignKey(Review, verbose_name='Выберите рецензию', null=False, on_delete=models.CASCADE)
    comment = models.TextField(verbose_name='Введите комментарий', blank=True, null=True)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return f'{self.review} - [{self.comment}]'

