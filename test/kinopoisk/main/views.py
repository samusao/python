from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.generics import *

from .serializers import MovieSerializer, GenresSerializer, ReviewSerializer, CommentSerializer
from .models import Movie, Genres, Review, Comment


class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class GenresViewSet(ListAPIView):
    queryset = Genres.objects.all()
    serializer_class = GenresSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer