from rest_framework.serializers import *

from .models import *


class GenresSerializer(ModelSerializer):
    class Meta:
        model = Genres
        fields = '__all__'


class MovieSerializer(ModelSerializer):
    genre = GenresSerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = '__all__'


class ReviewSerializer(ModelSerializer):

    class Meta:
        model = Review
        fields = '__all__'


class CommentSerializer(ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'