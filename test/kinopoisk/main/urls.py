from django.contrib import admin
from django.urls import path
from rest_framework import routers

from .views import *

message_router = routers.DefaultRouter()

message_router.register(r'movie', MovieViewSet, basename='kinopoisk')
message_router.register(r'review', ReviewViewSet, basename='kinopoisk')
message_router.register(r'comment', CommentViewSet, basename='kinopoisk')

urlpatterns = [
    path(r'genres/', GenresViewSet.as_view())
]

urlpatterns += message_router.urls