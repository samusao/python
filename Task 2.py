import asyncio
import random

max_elements = 10

tasks = []

queue = asyncio.Queue()


async def producer(x):
    while True:
        rand = random.randint(a=1, b=10000)
        print('Thread {} produced number {}'.format(x, rand))
        if len(tasks) < max_elements:
            await queue.put(rand)
            tasks.append(rand)
        else:
            print("Overflow. Producer {} waiting".format(x))

        await asyncio.sleep(random.random() * 5.0)


async def consumer(x):
    while True:
        if not len(tasks) == 0:
            await queue.get()
            pops = tasks.pop(0)
            print('Thread {} consumed number {}'.format(x, pops))
        else:
            print("Underflow. Consumer {} waiting".format(x))

        await asyncio.sleep(random.random() * 5.0)


loop = asyncio.get_event_loop()

for i in range(2):
    loop.create_task(producer(i))

for i in range(10):
    loop.create_task(consumer(i))

loop.run_forever()
