import random
from threading import Thread, Lock
from time import sleep


class dinner(Thread):
    running = True

    def __init__(self, name, left, right):
        Thread.__init__(self)
        self.name = name
        self.left = left
        self.right = right

    def run(self):
        while self.running:
            sleep(random.randint(a=1, b=5))
            print('{} голодный'.format(self.name))
            self.dine()

    def dine(self):
        Leftfork = self.left
        Rightfork = self.right
        while self.running:
            Leftfork.acquire(True)
            lockRight = Rightfork.acquire(False)
            if lockRight:
                break
            Leftfork.release()
            print('Для {} вилки заняты'.format(self.name))
            Leftfork, Rightfork = Rightfork, Leftfork
            sleep(5)
        else:
            return

        self.dining()
        Rightfork.release()
        Leftfork.release()


    def dining(self):
        print('{} начал кушать'.format(self.name))
        sleep(random.randint(a=3, b=10))
        print('{} закончил еду и думает'.format(self.name))


def main():

    Forks = ('Первая вилка', 'Вторая вилка', 'Третья вилка', 'Четвертая вилка', 'Пятая вилка')
    LockForks = [Lock() for n in Forks]


    first = dinner("1 - first", LockForks[0], LockForks[1])
    second = dinner("2 - second", LockForks[1], LockForks[2])
    third = dinner("3 - third", LockForks[2], LockForks[3])
    fourth = dinner("4 - fourth", LockForks[3], LockForks[4])
    fifth = dinner("5 - fifth", LockForks[4], LockForks[0])
    dinner.running = True

    first.start()
    second.start()
    third.start()
    fourth.start()
    fifth.start()

    sleep(30)
    dinner.running = False

main()